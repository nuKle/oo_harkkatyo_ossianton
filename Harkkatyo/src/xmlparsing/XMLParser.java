package xmlparsing;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/* Class XMLParser creates and contains SAXParser and its handler which can parse smartpost XML
in two modes. Follows  singleton pattern  */
public class XMLParser {
    SAXParserFactory parserFactory;
    SAXParser parser;
    
    public XMLParser() {
        try {
            parserFactory = SAXParserFactory.newInstance();
            parser = parserFactory.newSAXParser();
        } catch (ParserConfigurationException | SAXException e){
            System.err.println(e.getMessage());
        }
    }
 
    /* Sets handler values for different handling modes and then calls parser*/
    public void parseXML() {
       try{URL url = new URL("http://smartpost.ee/fi_apt.xml");
            InputStream inputstream = url.openStream();
            parser.parse(inputstream, new SAXHandlerCities());
            inputstream.close();
        }catch (IOException | SAXException e){
            System.err.println(e.getMessage());
        }        
    }
    
    public void parseXML(String city) {
        try{URL url = new URL("http://smartpost.ee/fi_apt.xml");
            InputStream inputstream = url.openStream();
            parser.parse(inputstream, new SAXHandlerSmartPosts(city));
            inputstream.close();
        }catch (IOException | SAXException e){
            System.err.println(e.getMessage());
        }
    }
}
