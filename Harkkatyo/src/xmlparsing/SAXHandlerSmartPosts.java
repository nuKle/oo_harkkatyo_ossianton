package xmlparsing;

import datadistributor.DataDistributor;
import smartpost.SmartPost;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/*SAXHandler class has methods which are called when xml start and end tags are
 met while parsing. source: http://www.java-samples.com/showtutorial.php?tutorialid=152
 */
class SAXHandlerSmartPosts extends DefaultHandler {
    /* Contains the data between xml tags */
    private String content;
    /* Wanted city */
    private String city = "";
    /* Contains last found postal code */
    private String postalhelper;
    /* capture particular city's data */
    private boolean capturemode = false;
    SmartPost sp = null;

    public SAXHandlerSmartPosts(String c) {
        city = c;
    }
//    public void setCity(String c){
//        city = c;
//    }
    /* This is called when parser finds xml start tag*/

    @Override
    public void startElement(String uri, String localName,
            String qName, Attributes attributes) {
    }
    /* This method is called when parser finds xml ending tag */

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (capturemode) {
            switch (qName) {
                case "place":
                    sp.setPostal(postalhelper);
                    DataDistributor.getInstance().addSmartPost(sp);
                    capturemode = false;
                    break;
                case "address":
                    sp.setAddress(content);
                    break;
                case "availability":
                    sp.setOpenhours(content);
                    break;
                case "postoffice":
                    sp.setOffice(content);
                    break;
                case "lat":
                    sp.setLat(content);
                    break;
                case "lng":
                    sp.setLng(content);
                    break;
            }
        } /* Found wanted city in XML >> capture this place's data */
        else if ("city".equals(qName)) {
            if (content.equals(city)) {
                capturemode = true;
                sp = new SmartPost();
                sp.setCity(city);
            }
        } 
        else if ("code".equals(qName)) {
            postalhelper = content;
        }
    }
    
    /* This is called when handler is moving between xml tags */
    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
