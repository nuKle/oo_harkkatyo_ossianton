package xmlparsing;


import datadistributor.DataDistributor;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/*SAXHandler class has methods which are called when xml start and end tags are
met while parsing. source: http://www.java-samples.com/showtutorial.php?tutorialid=152
*/

class SAXHandlerCities extends DefaultHandler {
    /* Contains the data between xml tags */
    private String content;
    
    /* This is called when parser finds xml start tag*/
    @Override
    public void startElement(String uri, String localName,
            String qName, Attributes attributes) {
    }
    
    /* This method is called when parser finds xml ending tag */
    @Override
    public void endElement(String uri, String localName, String qName) {
        /* This adds all the cities in XML */
        if ("city".equals(qName)) {
            DataDistributor.getInstance().addCity(content);
        }
    }
    
    /* This is called when handler is moving between xml tags */
    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}
