package minigame3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
/**
 *
 * @author Anton
 */
public class FXMLDocumentControllerMiniGame3 implements Initializable {
    @FXML
    private Rectangle box;
    @FXML
    private AnchorPane mainPane;
    @FXML
    private Region region;
    @FXML
    private Button startGameBtn;
    @FXML
    private Label infoTextLabel;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label endGameLabel;
    @FXML
    private Button endGameBtn;
    @FXML
    private Group player;
    @FXML
    private Group enemy2;
    @FXML
    private Group enemy1;    
    
    /**/
    Stage dialogStage;
    /* return value to main window;whether play won or lost*/
    boolean finished = false;
    /* source and destination cities */
    ArrayList<String> cities = new ArrayList<>();
    /* Animation timer instance, calls funtions when new frame is drawn*/
    Timer timer;
    /* whether player is controllable */
    boolean controllable = false;
    int enemySpeed = 10;
    /* how many people passed*/
    int peoplecounter = 0;
    int wincondition = 30;

    
    private class Timer extends AnimationTimer {
        /*gets called every frame*/
        @Override
        public void handle(long now) {
            checkCollision();
            moveEnemy(1);
            moveEnemy(2);
        }
    }
    /*checks collisions and ends game if collision happened*/
    private void checkCollision(){
        Bounds hitboxPlayer = player.getBoundsInParent();
        Bounds hitboxEnemy1 = enemy1.getBoundsInParent();
        Bounds hitboxEnemy2 = enemy2.getBoundsInParent();
        if (hitboxPlayer.intersects(hitboxEnemy1) || hitboxPlayer.intersects(hitboxEnemy2)){
            System.out.println("collision");
            gameEndAction(false);
        }
        
    }
    
    /* moves player based on mouse x coordinate*/
    @FXML
    private void mouseMovedAction(MouseEvent event) {
        if (controllable){
            player.setLayoutX(event.getSceneX()-375);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            endGameLabel.setVisible(false);
    }
    
    /*set info message*/
    public void setCities(String s, String d){
            infoTextLabel.setText("TIMO-miehen pitäisi viedä 2. luokan paketti paikasta "
                    +s.toLowerCase()+" paikkaan "+d.toLowerCase()
                    + ". 2. luokan paketissa voi olla särkyvää tavaraa, joten "
                    + "hänen on oltava varovainen. TIMO-mies on juuri noutanut "
                    + "paketin lähtöautomaatista. Joulun ajan ihmisryntäys posti"
                    + "toimipisteessä on koitua paketin kohtaloksi. Väistele ihmisiä"
                    + " hiirtä liikuttamalla päästäksesi matkaan.");
    }
    
    /* starts timer and hides info panel*/
    @FXML
    private void startGameAction(ActionEvent event) {
        timer = new Timer();
        timer.start();
        controllable = true;
        startGameBtn.setVisible(false);
        infoTextLabel.setVisible(false);
    }
    
    /* stops timer and shows end game messages based on victory or defeat */
    private void gameEndAction(boolean win){
        timer.stop();
        controllable= false;
        if (win){
            endGameLabel.setText("Paketti viedään turvassa perille. Voitit pelin. ");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = true;
        }else{
            endGameLabel.setText("Törmäsit ja paketti hajosi, vastaanottajan joulu on pilalla. Hävisit pelin.");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = false;
        }
    }
    
    /*close window*/
    @FXML
    private void endButtonPressed(ActionEvent event) {
        dialogStage.close();
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public boolean isFinished() {
        return finished;
    }
    
    /*moves both enemies. calls for new placement if enemy goes past bottom of screen*/
    private void moveEnemy(int i){
        if (i == 1) {
            enemy1.setLayoutY(enemy1.getLayoutY() +enemySpeed);
            if(enemy1.getBoundsInParent().getMinY() > region.getHeight()) {
                newEnemyPlacement(1);
            }
        }
        else {
            enemy2.setLayoutY(enemy2.getLayoutY() + enemySpeed);
            if(enemy2.getBoundsInParent().getMinY() > region.getHeight()) {
                newEnemyPlacement(2);
            }
        }
    }
    /* moves enemies to random location on top of screen*/
    private void newEnemyPlacement(int i){
        peoplecounter += 1;
        if (peoplecounter >= wincondition){
            gameEndAction(true);
        }
        if (peoplecounter < wincondition-1){
            Random rand = new Random();
            int r = rand.nextInt(550);
            switch (i){
                case 1:

                    enemy1.setLayoutX(r);
                    enemy1.setLayoutY(-500);
                    break;
                case 2:
                    enemy2.setLayoutX(r);
                    enemy2.setLayoutY(-500);
                    break;
            }
        }else{
            switch (i){
                case 1:
                    enemy1.setLayoutY(-10000);
                    break;
                case 2:
                    enemy2.setLayoutY(-10000);
                    break;
            }            
        }
    }
}