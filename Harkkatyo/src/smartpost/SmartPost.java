package smartpost;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/* Class for smartpost, contains all the info taken from the xml as attributes, getters
and setters for attributes*/
public class SmartPost {

    private String postal;
    private String city;
    private String address;
    private String openhours;
    private String office;
    private String lat;
    private String lng;
     
    public SmartPost() {
        
    }
    
    public void setPostal(String postal) {
        this.postal = postal;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setOpenhours(String openhours) {
        this.openhours = openhours;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
    
    public String getPostal() {
        return postal;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getOpenhours() {
        return openhours;
    }

    public String getOffice() {
        return office;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
    
    public String getAddressFull(){
        return (address+" " + postal + " " + city);
    }
    /* overridden toString method to show office info in comboboxes */
    @Override
    public String toString(){
        return office;
    }
}
