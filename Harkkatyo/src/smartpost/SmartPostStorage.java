package smartpost;


import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/* Singleton class contains lists that hold cities and smartposts added to map. */
public class SmartPostStorage {
    /* Contains all cities which have smartposts */
    private ArrayList<String> cities = null;
    /* Contains all smartposts added to map */
    private ArrayList<SmartPost> posts = null;
    /* Helper list that is used when adding smartposts */
    private ArrayList<SmartPost> recentlyadded = null;
    
    public  SmartPostStorage() {
        /* Initialization of lists */
        cities = new ArrayList<>();
        posts = new ArrayList<>();
        recentlyadded = new ArrayList<>();
    }
    
    /* Helper method for checking if smartpost is already in the posts list
    (if user added same city's smartposts multiple times) */
    private boolean containsOffice(String office){
        for(SmartPost post : posts){
            if (post.getOffice().equals(office)){
                return true;
            }
        }
        return false;
    }
    /* Method for adding smartposts to lists */
    public void addSmartPost(SmartPost sp) { 
        if (!containsOffice(sp.getOffice())){
            recentlyadded.add(sp);
            posts.add(sp);
        }
    }
    /* Clearing helperlist after map marking operation is done */
    public void clearRecentlyAdded(){
        recentlyadded.clear();
    }
    /* adding cities to cities list */
    public void addCity(String c){
        if (!cities.contains(c)){
            cities.add(c);
        }
    }
    /* Getters for lists */
    public ArrayList<String> getCities(){
        Collections.sort(cities);
        return cities;
    }
    public ArrayList<SmartPost> getPosts(){
        return posts;
    }
    public ArrayList<SmartPost> getRecentlyAdded(){
        return recentlyadded;
    }
}
