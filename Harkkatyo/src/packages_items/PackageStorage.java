package packages_items;


import smartpost.SmartPost;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public class PackageStorage {
    private ArrayList<Package> package_array = new ArrayList<>();
    
    public PackageStorage(){
        
    }
    
    public ArrayList<Package> getPacks(){
        return package_array;
    }
    
    /* creates right kind of pack class instance and checks if right kind of item */
    public String addPackage(int i,Item item,SmartPost s, SmartPost d){
        Package pack;
        switch (i){
            case 1:
                pack = new FirstClass(item, s, d);
                break;
            case 2:
                pack = new SecondClass(item, s, d);
                break;
            case 3:
                pack = new ThirdClass(item, s, d);
                break;
            default:
                /* doesnt end up here ever*/
                pack = new FirstClass(item,s,d);
                break;
        }
        String reportString = pack.checkClass();

        if (reportString.equals("OK")){
            System.out.println("Oikeanlainen");
            package_array.add(pack);
            return reportString;
        } 
        else {
            System.out.println("esine vääränlainen");
            return reportString;
        }
        
    }
    
}
