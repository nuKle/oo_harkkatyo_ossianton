package packages_items;

import smartpost.SmartPost;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public class FirstClass extends Package{
    /* max distance first class pack can travel */
    private final int distance = 150;

    public FirstClass(Item item, SmartPost SPBegin, SmartPost SPEnd) {
        super(item, SPBegin, SPEnd);
    }
    /*Checking pack requirements */
    @Override
    public String checkClass(){
        if (item.getFragile()){
            return "Esine särkyvää. Valitse toinen pakettiluokka.";
        }
        return "OK";
    }
    
    @Override
    public String toString(){
        return("1.lk, "+SPBegin.getCity()+" -> "+SPEnd.getCity());
    }
    
    
}
