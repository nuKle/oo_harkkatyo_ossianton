package packages_items;

import smartpost.SmartPost;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public abstract class Package {
    /* pack holds item and knows its start and destination SPs*/
    protected Item item;
    protected SmartPost SPBegin;
    protected SmartPost SPEnd;

    public Package(Item item, SmartPost SPBegin, SmartPost SPEnd){
        this.item = item;
        this.SPBegin = SPBegin;
        this.SPEnd = SPEnd;
    }
    
    public Item getItem() {
        return item;
    }

    public SmartPost getSPBegin() {
        return SPBegin;
    }

    public SmartPost getSPEnd() {
        return SPEnd;
    }

    public abstract String checkClass();
    
}
