package packages_items;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * 
 * @author Ossi
 */

/* item attributes, getters and setters for those */
public class Item {
    private String name;
    private double depth; 
    private double width;
    private double height;
    private double mass;
    private boolean fragile = false;


    public Item(String n, double d, double w, double h,double m,boolean f){
        name = n;
        depth = d;
        width = w;
        height = h;
        mass = m;
        fragile = f;
        
    }

    public String getName() {
        return name;
    }

    public double getDepth() {
        return depth;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getMass() {
        return mass;
    }
    public boolean getFragile() {
        return fragile;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    
}
