package packages_items;

import smartpost.SmartPost;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public class ThirdClass extends Package{

    public ThirdClass(Item item, SmartPost SPBegin, SmartPost SPEnd) {
        super(item, SPBegin, SPEnd);
    }
    /*Checking pack requirements */
    @Override 
    public String checkClass(){
        System.out.println("3 luokan tarkastus");
        if (item.getFragile() == true){
            return "Särkyvää. Valitse toinen luokka.";
        }
        return "OK";
    }
    @Override
    public String toString(){
        return("3.lk, "+SPBegin.getCity()+" -> "+SPEnd.getCity());
    }
}
