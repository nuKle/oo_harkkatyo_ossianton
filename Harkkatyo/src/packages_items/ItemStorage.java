package packages_items;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public class ItemStorage {
    ArrayList<Item> item_array = new ArrayList<Item>();
    
    /*Default items to list */
    public ItemStorage(){
        item_array.add(new Item("kirja", 3, 10, 20, 0.4,false));
        item_array.add(new Item("kristallipallo", 25, 25, 28, 10,true));
        item_array.add(new Item("televisio", 4, 120, 78, 7,true));
        item_array.add(new Item("jalkalamppu", 35, 35, 180, 5,false));
        item_array.add(new Item("sohva", 140, 280, 100, 60,true));
    }
    
    public void addItem(Item item){
        item_array.add(item);
    }
    
    public ArrayList<Item> getItems(){
        return item_array;
    }
}
