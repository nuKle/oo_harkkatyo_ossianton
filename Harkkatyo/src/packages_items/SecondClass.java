package packages_items;

import smartpost.SmartPost;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Ossi
 */
public class SecondClass extends Package{
    
    public SecondClass(Item item,  SmartPost SPBegin, SmartPost SPEnd) {
        super(item, SPBegin, SPEnd);
    }

    /*Checking pack requirements */
    @Override
    public String checkClass(){
        System.out.println("2 luokan tarkastus");
        //tähän väliin jonkinlainen tilavuudentarkastelu. Jos ehdot ei täyty
        //niin check = false
        if (item.getDepth() > 60) {
            return item.getDepth()+"cm, liian syvä. Valitse toinen pakettiluokka.";
        }
        else if (item.getHeight() > 36) {
            return item.getHeight()+"cm, liian korkea. Valitse toinen pakettiluokka.";
        }
        else if (item.getWidth() > 60) {
            return item.getWidth()+"cm, liian leveä. Valitse toinen pakettiluokka.";
        } 
        else {    
            return "OK";
        }
    }
    
    @Override
    public String toString(){
        return("2.lk, "+SPBegin.getCity()+" -> "+SPEnd.getCity());
    }
    
}
