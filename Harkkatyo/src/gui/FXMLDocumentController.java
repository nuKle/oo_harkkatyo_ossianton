package gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import datadistributor.DataDistributor;
import smartpost.SmartPost;
import packages_items.FirstClass;
import packages_items.SecondClass;
import packages_items.Item;
import packages_items.Package;
import minigame1.FXMLDocumentControllerMiniGame1;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import minigame2.FXMLDocumentControllerMiniGame2;
import minigame3.FXMLDocumentControllerMiniGame3;

/**
 *
 * @author Anton
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView mapView;
    @FXML
    private ComboBox<Package> packsCombo;
    @FXML
    private ComboBox<String> citiesCombo;
    @FXML
    private Button createPackBtn;
    @FXML
    private Button closeBtn;
    @FXML
    private AnchorPane rightPane;
    @FXML
    private ComboBox<Item> itemsCombo;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemWidthField;
    @FXML
    private TextField itemHeightField;
    @FXML
    private TextField itemDepthField;
    @FXML
    private TextField itemMassField;
    @FXML
    private CheckBox fragileCheckbox;
    @FXML
    private RadioButton firstClassRadio;
    @FXML
    private RadioButton secondClassRadio;
    @FXML
    private RadioButton thirdClassRadio;
    @FXML
    private ComboBox<SmartPost> startSPCombo;
    @FXML
    private ComboBox<SmartPost> destSPCombo;
    @FXML
    private ComboBox<String> startCityCombo;
    @FXML
    private ComboBox<String> destCityCombo;
    @FXML
    private Button AddSPBtn;
    @FXML
    private Button sendPackBtn;
    @FXML
    private Button clearBtn;
    @FXML
    private Button wrapPackBtn;
    @FXML
    private Button createItemBtn;
    @FXML
    private ToggleGroup classesGroup;
    @FXML
    private Label packingLabel;
    @FXML
    private CheckBox gamesCheckbox;    
    @FXML
    private Label errorLabel;

    /*DD instance*/
    DataDistributor dd;

    /* On GUI initialization */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /* userdata for checking which radiobtn is active */
        firstClassRadio.setUserData(1);
        secondClassRadio.setUserData(2);
        thirdClassRadio.setUserData(3);

        /* singleton instance of DataDistributor */
        dd = DataDistributor.getInstance();
        /* Hide pane for creating packs */
        rightPane.setPrefWidth(0);
        /*Show map*/
        mapView.getEngine().load(getClass()
                .getResource("index.html").toExternalForm());
        /* Show all cities which have smartposts in combobox */
        for (String city : dd.getCities()) {
            citiesCombo.getItems().add(city);
        }
        /* Example items to itemsCombo */
        for (Item item : dd.getItems()) {
            itemsCombo.getItems().add(item);
        }

    }
    
    /* When "Lisää kartalle"(AddSPBtn) is clicked, this method runs */
    @FXML
    private void AddSPAction(ActionEvent event) throws Exception {
        /* contains info to show when marker on map is clicked */
        String info;
        /* Find city's SmartPosts from XML */
        ArrayList<SmartPost> addedPosts = dd.parseSP(citiesCombo.getValue());
        /*Add found SmartPosts to map*/
        for (SmartPost sp : addedPosts) {
            info = sp.getAddressFull() + "<br>" + sp.getOffice() + "<br>" + sp.getOpenhours();
            String function = "document.goToLocation('" + sp.getAddressFull() + "','"
                    + info + "','red')";
            mapView.getEngine().executeScript(function);
        }
        /*Add found SmartPosts to comboboxes in pack creation pane*/
        for (SmartPost sp : dd.getSmartPosts()) {
            if (!startCityCombo.getItems().contains(sp.getCity())) {
                startCityCombo.getItems().add(sp.getCity());
                destCityCombo.getItems().add(sp.getCity());
            }
        }
        System.out.println(dd.getSmartPosts());
    }
    
    /* When "Lähetä paketti"(sendPackButton) is clicked, this method runs */
    @FXML
    private void sendPackAction(ActionEvent event) {
        /* color to draw path */
        String color;
        int packClass;
        boolean gamewon = true;
        /* coordinates to send to javascript*/
        ArrayList<String> array = new ArrayList<>();
        Package pack = packsCombo.getValue();
        if (pack != null) {
            errorLabel.setText("");
            /* selected pack's start and end coordinates into array */
            array.add(pack.getSPBegin().getLat());
            array.add(pack.getSPBegin().getLng());
            array.add(pack.getSPEnd().getLat());
            array.add(pack.getSPEnd().getLng());
            /*define color and class for JS function, and possibly launch game */
            if (pack instanceof FirstClass) {
                color = "red";
                packClass = 1;
                if (gamesCheckbox.isSelected()) {
                    gamewon = startGameAction(packClass, pack.getSPBegin().getCity(), pack.getSPEnd().getCity());
                }
            } else if (pack instanceof SecondClass) {
                color = "blue";
                packClass = 2;
                if (gamesCheckbox.isSelected()) {
                    gamewon = startGameAction(packClass, pack.getSPBegin().getCity(), pack.getSPEnd().getCity());
                }
            } else {
                color = "green";
                packClass = 3;
                if (gamesCheckbox.isSelected()) {
                    gamewon = startGameAction(packClass, pack.getSPBegin().getCity(), pack.getSPEnd().getCity());
                }
            }
            if (gamewon) {
                /*draw path to map*/
                String function = "document.createPath(" + array + ",'" + color + "'," + packClass + ")";
                mapView.getEngine().executeScript(function);
            }
        } else {
            errorLabel.setText("Muistithan valita paketin!");
        }
    }
    
    /*Clears map from paths when "Tyhjennä reitit"(clearBtn) is clicked*/
    @FXML
    private void clearPathsAction(ActionEvent event) {
        mapView.getEngine().executeScript("document.deletePaths()");
    }
    
    /* Next two methods hide or show pack creation panel */
    @FXML
    private void closeRightPaneAction(ActionEvent event) {
        rightPane.setPrefWidth(0);
    }

    @FXML
    private void openRightPaneAction(ActionEvent event) {
        rightPane.setPrefWidth(300);
    }

    /* This method updates smartpost comboboxes when certain city is chosen 
     as source or destination location in pack creation panel */
    @FXML
    private void updateSPComboAction(ActionEvent event) {
        /* combo holds either source or destination SmartPost combobox
         depending on which city combobox was changed */
        ComboBox<SmartPost> combo;
        ArrayList<SmartPost> posts = dd.getSmartPosts();
        ComboBox<String> source = (ComboBox<String>) event.getSource();
        if (source.equals(startCityCombo)) {
            combo = startSPCombo;
            combo.getItems().clear();
        } else {
            combo = destSPCombo;
            combo.getItems().clear();
        }
        /* Appropriate smartposts to appropriate combobox */
        for (SmartPost post : posts) {
            if (source.getValue().equals(post.getCity())) {
                combo.getItems().add(post);
            }
        }
    }

    /* This is tweak that allows user to navigate comboboxes by pressing
     letters on keyboard. Combobox jumps to first city starting with that letter */
    @FXML
    private void ComboKeyNavigateAction(KeyEvent event) {
        String pressed = event.getText().toUpperCase();
        if (event.getCode().isLetterKey()) {
            for (String s : citiesCombo.getItems()) {
                if (s.startsWith(pressed)) {
                    citiesCombo.setValue(s);
                    citiesCombo.hide();
                    break;
                }
            }
        }
    }
    /* starts minigame window */
    private boolean startGameAction(int packClass, String start, String end) {

        try {
            FXMLLoader loader;
            Scene scene;
            Stage game = new Stage();
            /* selecting game depending on pack class*/
            switch (packClass) {
                case 1:
                    game.setFullScreen(true);
                    loader = new FXMLLoader(getClass().getResource("/minigame2/FXMLDocumentMiniGame2.fxml"));
                    scene = new Scene(loader.load());
                    FXMLDocumentControllerMiniGame2 controller2 = loader.getController();
                    controller2.setCities(start, end);
                    controller2.setDialogStage(game);
                    game.setScene(scene);
                    game.showAndWait();
                    return controller2.isFinished();
                case 2:
                    loader = new FXMLLoader(getClass().getResource("/minigame3/FXMLDocumentMiniGame3.fxml"));
                    scene = new Scene(loader.load());
                    FXMLDocumentControllerMiniGame3 controller3 = loader.getController();
                    controller3.setCities(start, end);
                    controller3.setDialogStage(game);
                    game.setScene(scene);
                    game.showAndWait();
                    return controller3.isFinished();
                case 3:
                    loader = new FXMLLoader(getClass().getResource("/minigame1/FXMLDocumentMiniGame1.fxml"));
                    scene = new Scene(loader.load());
                    FXMLDocumentControllerMiniGame1 controller1 = loader.getController();
                    controller1.setCities(start, end);
                    controller1.setDialogStage(game);
                    game.setScene(scene);
                    game.showAndWait();
                    return controller1.isFinished();
            }

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        /*never*/
        return (false);
    }
    @FXML
    private void wrapPackAction(ActionEvent event) {
        /* Getting values from comboboxes */
        Item item = itemsCombo.getValue();
        SmartPost start = startSPCombo.getValue();
        SmartPost dest = destSPCombo.getValue();
        System.out.println(item);
        if (start != null && dest != null && item != null) {
            /* Selected pack class radiobutton */
            Toggle selected = classesGroup.getSelectedToggle();
            switch ((int) selected.getUserData()) {
                case 1:
                    /* 1st class: must check if smartposts' distance exceeds 150km */
                    double length = (Double) mapView.getEngine().executeScript(
                            "document.getPathLength(" + start.getLat() + ","
                            + start.getLng() + "," + dest.getLat() + "," + dest.getLng() + ")");
                    if (length > 150) {
                        packingLabel.setText("Liian pitkä matka smartpostien välillä: " + length
                                + "km. Valitse toinen pakettiluokka.");
                    } else {
                        packingLabel.setText(dd.addPack(1, item, start, dest));
                    }
                    break;
                case 2:
                    packingLabel.setText(dd.addPack(2, item, start, dest));
                    break;
                case 3:
                    packingLabel.setText(dd.addPack(3, item, start, dest));
                    break;
            }
            /* update combobox */
            packsCombo.getItems().clear();
            for (packages_items.Package pack : dd.getPackages()) {
                packsCombo.getItems().add(pack);
            }

        } else {
            packingLabel.setText("Tarkista, että olet valinnut lähetettävän"
                    + " esineen ja automaatit");
        }
    }

    /* Method runs when "Luo esine" (createItemBtn) is clicked.
     Collects data from textfields, creates new item, adds that item to itemlist
     in datadistributor and updates combobox */
    @FXML
    private void createItemAction(ActionEvent event) {

        /* errorchecking */
        if (itemNameField.getText().isEmpty()
                || itemMassField.getText().isEmpty()
                || itemDepthField.getText().isEmpty()
                || itemWidthField.getText().isEmpty()
                || itemHeightField.getText().isEmpty()) {
            packingLabel.setText("Täytä kaikki kentät.");
        } else {
            packingLabel.setText("");
            /* gather data from textfields, create item and add to combobox */
            String name = itemNameField.getText();
            double mass = Double.parseDouble(itemMassField.getText());
            double d = Double.parseDouble(itemDepthField.getText());
            double w = Double.parseDouble(itemWidthField.getText());
            double h = Double.parseDouble(itemHeightField.getText());
            boolean fragile = fragileCheckbox.isSelected();
            Item item = new Item(name, d, w, h, mass, fragile);
            dd.addItem(item);
            itemsCombo.getItems().add(item);
            itemsCombo.setValue(item);

        }
    }
}
