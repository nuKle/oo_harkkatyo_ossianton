package datadistributor;


import xmlparsing.XMLParser;
import smartpost.SmartPost;
import smartpost.SmartPostStorage;
import packages_items.Item;
import packages_items.Package;
import packages_items.PackageStorage;
import packages_items.ItemStorage;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anton
 */

/* Class that sends lists to GUI and adds to lists. Singleton. */
public class DataDistributor {
    /* instances of storage classes and XMLParser and singleton instance*/
    private SmartPostStorage posts;
    private ItemStorage items;
    private PackageStorage packages;
    private XMLParser parser;
    private static DataDistributor instance = null;
    
    private DataDistributor() {
        posts = new SmartPostStorage();
        parser = new XMLParser();
        items = new ItemStorage();
        packages = new PackageStorage();
    }
    /* singleton instance getter*/
    public static DataDistributor getInstance() {
        if (instance == null){
            instance = new DataDistributor();
            instance.parseCities();
        }
        return instance;
    }
    
    public ArrayList<Package> getPackages(){
        return packages.getPacks();
    }
    /* this tries to add pack to list, return string is whether it succeeded or not */
    public String addPack(int i,Item item, SmartPost s, SmartPost d) {
        return(packages.addPackage(i,item, s, d));
    }
    
    public ArrayList<Item> getItems(){
        return items.getItems();
    }
    
    public void addItem(Item item) {
        items.addItem(item);
    }


    public ArrayList<SmartPost> getSmartPosts() {
        return posts.getPosts();
    }
    
    public ArrayList<String> getCities() {
       return posts.getCities();
    }
    /* get all cities from XML */
    public void parseCities(){
        parser.parseXML();
    }
    /* get SPs from XML and return them */
    public ArrayList<SmartPost> parseSP(String city){
        posts.clearRecentlyAdded();
        parser.parseXML(city);
        return posts.getRecentlyAdded();
        
    }
    /* next two are called by Sax handlers to add to lists */
    public void addSmartPost(SmartPost sp){
        posts.addSmartPost(sp);
    }
    
    public void addCity(String city){
        posts.addCity(city);
    }    
}
