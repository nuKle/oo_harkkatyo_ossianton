package minigame1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
/**
 *
 * @author Anton
 */
public class FXMLDocumentControllerMiniGame1 implements Initializable {
    @FXML
    private Rectangle box;
    @FXML
    private Rectangle wall;
    @FXML
    private AnchorPane mainPane;
    @FXML
    private Region region;
    @FXML
    private Rectangle rightFoot;
    @FXML
    private Rectangle leftFoot;
    @FXML
    private Button startGameBtn;
    @FXML
    private Label infoTextLabel;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label endGameLabel;
    @FXML
    private Button endGameBtn;
    
    /**/
    Stage dialogStage;
    /* whether player won or lost, used in main window */
    boolean finished = false;
    ArrayList<String> cities = new ArrayList<>();
    Timer timer;
    /* this is switched when box collides with wall*/
    int turn = 1;
    /* -1 for box movement to left, +1 to right, 0 when not moving*/
    int direction = 0;
    /* used to swap if foot is moving up or down */
    int feet = 1;
    int wallspeed = 5;
    int boxspeed = 15;
    /*player score*/
    int score = 0;
    /*win score */
    int maxscore = 15;
    /* whether you can move the player or not*/
    boolean controllable = true;

    private class Timer extends AnimationTimer {
        /* called every frame */
        @Override
        public void handle(long now) {
            checkCollision();
            animateFeet();
            /* moving box to correct direction*/
            if (direction == 1){
                box.setLayoutX(box.getLayoutX() + (boxspeed * turn));
            } else if (direction == -1) {
                box.setLayoutX(box.getLayoutX() + (boxspeed * turn));
            }
            /* moving walls */
            wall.setLayoutY(wall.getLayoutY() + wallspeed);
        }
    }
    
    private void checkCollision(){
        /* collision of wall and box */
        Bounds boxbound = box.getBoundsInParent();
        if (boxbound.intersects(wall.getBoundsInParent())){
            System.out.println("Collision");
            score += 1;
            scoreLabel.setText(score+"/"+maxscore);
            turn = -(turn);
        }
        /* game fails if box moves out of screen*/
        if (boxbound.getMaxX() >= region.getWidth()){
            System.out.println("Game over");
            gameEndAction(false);
        }
        if (boxbound.getMaxX()< 0) {
            System.out.println("Game over");
            gameEndAction(false);
        }
        /* if wall moves off screen, call newWallPlacement */
        if (wall.getBoundsInParent().getMinY() >= region.getHeight()) {
            newWallPlacement();
        }
        
    }
    /* moving feet */
    private void animateFeet(){
        if (leftFoot.getLayoutY() <= 630){
            feet = 1;
        }
        if (leftFoot.getLayoutY() >= 660 ) {
            feet = -1;
        }
        leftFoot.setLayoutY(leftFoot.getLayoutY()+ (feet));
        rightFoot.setLayoutY(rightFoot.getLayoutY()+ (-feet));
    }
    
    /* checking key presses */
    @FXML
    private void inputAction(KeyEvent event) {
        /* throws right */
        if (event.getCode() == KeyCode.RIGHT && controllable) {
            System.out.println("Painoit oikealle");
            direction = 1;
            turn = 1;
            controllable = false;
        } else if (event.getCode() == KeyCode.UP) {
            System.out.println("Painoit ylös");
            /* catches box if pressed up and box somewhere in front of player */
            if (226 < box.getLayoutX() && box.getLayoutX() < 436) {
                direction = 0;
                if (score == maxscore){
                    gameEndAction(true);
                }
                
                controllable = true;
            }    
        }  
        /* throws left */
        else if (event.getCode() == KeyCode.LEFT && controllable) {
            System.out.println("Painoit vasemmalle"); 
            direction = -1;
            turn = -1;
            controllable = false;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

            endGameLabel.setVisible(false);
    }
    /* set info message */
    public void setCities(String s, String d){
            infoTextLabel.setText("TIMO-miehellä on ollut tänään huono päivä. "
                    + "Nyt hänen pitäisi viedä 3. luokan paketti paikasta "+s.toLowerCase()+" paikkaan "+d.toLowerCase()
                    + ". TIMO-mies on juuri hakenut paketin lähtöautomaatista "
                    + "ja on lähdössä matkaan. Auta TIMO-miestä purkamaan "
                    + "stressiä heittämällä pakettia seiniin, nimittäin "
                    + "liian stressaantuneena TIMO-mies ei jaksa "
                    + "kuljettaa pakettia perille. "
                    + "Heitä pakettia < >-nuolinäppäimillä "
                    + "ja ota kiinni painamalla ^-näppäintä.");
    }
    
    /* starts timer and hides info messages, random wall placement */
    @FXML
    private void startGameAction(ActionEvent event) {
        timer = new Timer();
        timer.start();
        startGameBtn.setVisible(false);
        infoTextLabel.setVisible(false);
        newWallPlacement();
    }
    
    /*stops timer and sets variables depending on whether played won or not*/
    private void gameEndAction(boolean win){
        timer.stop();
        if (win){
            endGameLabel.setText("Voitit pelin. TIMO jaksaa viedä paketin perille.");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = true;
        }else{
            endGameLabel.setText("Hävisit pelin. TIMO on liian stressaantunut eikä vie pakettia perille.");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = false;
        }
    }
    
    @FXML
    private void endButtonPressed(ActionEvent event) {
        dialogStage.close();
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public boolean isFinished() {
        return finished;
    }
    
    /* randomizes whether wall comes from left or right */
    private void newWallPlacement(){
        Random rand = new Random();
        int r = rand.nextInt(2);
        wallspeed += 1;
        switch (r){
            case 1:
                wall.setLayoutX(685);
                wall.setLayoutY(-200);
                break;
            case 0:
                wall.setLayoutX(50);
                wall.setLayoutY(-200);
                break;
        }
    }
}