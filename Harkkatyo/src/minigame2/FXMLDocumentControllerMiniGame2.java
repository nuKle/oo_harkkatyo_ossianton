package minigame2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
/**
 *
 * @author Anton
 */
public class FXMLDocumentControllerMiniGame2 implements Initializable {
    private Rectangle box;
    @FXML
    private AnchorPane mainPane;
    @FXML
    private Button startGameBtn;
    @FXML
    private Label infoTextLabel;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label endGameLabel;
    @FXML
    private Button endGameBtn;
    @FXML
    private Group timo;
    @FXML
    private Region region;
    @FXML
    private Rectangle leftFoot;
    @FXML
    private Rectangle rightFoot;
    @FXML    
    private Label speedLabel;
    
    /**/
    Stage dialogStage;
    boolean finished = false;
    ArrayList<String> cities = new ArrayList<>();
    Timer timer;
    /* player current speed */
    int speed = 0;
    int bestspeed = 0;
    /* whether player pressed left or right previously */
    int lastpressed = 0;
    String destCity;
    
    private class Timer extends AnimationTimer {
        /*gets called every frame*/
        @Override
        public void handle(long now) {
            /* when player reaches right edge, checks if high enough speed
            was achieved.*/
            if (timo.getLayoutX() > mainPane.getWidth()){
                if (bestspeed > 30) {
                    gameEndAction(true);
                }else {
                    gameEndAction(false);
                }
            }
            /*move player*/
            timo.setLayoutX(timo.getLayoutX()+speed);
            /* decrease speed so player has to press repeatedly */
            if (speed > 0){
                speed -= 1;
            }
            /* update max speed */
            if (speed > bestspeed) {
                bestspeed = speed;
                speedLabel.setText(speed + "kmh");
            }
        }
    }        
    
    /* checks key presses and increases speed accordingly.colors feet */
    @FXML
    private void inputAction(KeyEvent event) {
        if (event.getCode() == KeyCode.RIGHT) {
            if (lastpressed == 1) {
                leftFoot.setFill(Color.BLUE);
                rightFoot.setFill(Color.DARKBLUE);
                lastpressed = 0;
                speed += 6;
            }
        } else if (event.getCode() == KeyCode.LEFT) {
            if (lastpressed == 0){
                leftFoot.setFill(Color.DARKBLUE);
                rightFoot.setFill(Color.BLUE);
                lastpressed = 1;
                speed += 6;
            }
        } 
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

            endGameLabel.setVisible(false);
            leftFoot.setFill(Color.BLUE);
            rightFoot.setFill(Color.DARKBLUE);
    }
    /* set info message */
    public void setCities(String s, String d){
        destCity = d;
        infoTextLabel.setText("TIMO-miehen pitäisi viedä 1. luokan paketti"
                + " paikasta "+s.toLowerCase()+" paikkaan "+d.toLowerCase()
                + ". 1. luokan paketti on vietävä nopeasti perille, auta TIMO-"
                + "miestä kiihdyttämään liikkeelle painamalla < ja > -nuolinäppäimiä "
                + "mahdollisimman nopeasti vuorotellen. TIMO-miehen huippunopeuden "
                + "on oltava vähintään 30kmh, muuten TIMO-pomo katsoo toiminnan "
                + "laiskotteluksi, eikä pakettia viedä perille.");
    }
    
    /* starts timer and hides info screen */
    @FXML
    private void startGameAction(ActionEvent event) {
        timer = new Timer();
        speed = 0;
        timer.start();
        startGameBtn.setVisible(false);
        infoTextLabel.setVisible(false);
    }
    
    private void gameEndAction(boolean win){
        
        timer.stop();
        if (win){
            endGameLabel.setText("Voitit pelin. TIMO-mies jatkaa samaa vauhtia kohti "
            + destCity.toLowerCase()+":a");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = true;
        }else{
            endGameLabel.setText("'Liian hidasta!', huutaa TIMO-pomo. Hävisit pelin.");
            endGameLabel.setVisible(true);
            endGameBtn.setVisible(true);
            finished = false;
        }
    }
    
    @FXML
    private void endButtonPressed(ActionEvent event) {
        dialogStage.close();
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public boolean isFinished() {
        return finished;
    }
}